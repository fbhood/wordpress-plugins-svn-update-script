#!/usr/bin/env python3.7
import subprocess
import os
from get_plugins import get_wp_org_plugin_path

def trunk_and_commit(folder: str):
    # 1 add trunk files to the svn
    cwd = get_wp_org_plugin_path(folder)
    plugin_folder = "{}/{}".format(cwd, os.listdir(cwd)[0])
    #print("Printing plugin folder: {} ".format(plugin_folder))
    print("Executing svn add on: {}/trunk/*".format(plugin_folder))
    os.chdir(plugin_folder)
    subprocess.run(['svn', 'add', 'trunk/'])
    print("########## All Files added to the trunk ##########")

    # 2 commit changes with a message
    skip = input("Do you want to this repo's commit? [Y] to skip")
    if skip.lower() != "y":
        message = input("Add an inline commit message: ")
        while message == "":
            message = input("Please add a commit message: ")

        print("Commit message: {}".format(message))
        commit = input("Do you want to commit this message? [y/n]")

        while commit.lower() == "n":
            message = input("Please add a commit message: ")
        else:
            print("syincing ...")
            subprocess.run(['svn', 'ci', "-m'{}'".format(message)])
        print("########## Message Commited ########## ")


def trunk_tag_release_commit(folder):
    # 1. get the plugin wp.org folder
    wp_org_dir = get_wp_org_plugin_path(folder)
    # get plugin folder
    plugin_folder = "{}/{}".format(wp_org_dir, os.listdir(wp_org_dir)[0])
    print("Executing svn commands on {}".format(plugin_folder))
    # 2. add files to the svn

    print("Copying the files in {} to the folder {}/trunk ...".format(folder, plugin_folder))
    os.chdir(plugin_folder)
    subprocess.run(['svn', 'add', 'trunk/'])
    print("########## All Files added to the trunk ##########")

    # 3. copy truck into a new tag folder
    tags_available = os.listdir("{}/tags/".format(plugin_folder))
    print("Tags available {}".format(tags_available))
    print("create a new tag")
    tag = input("Type the tag here. ie: [tags/1.1] or [S] to skip")
    if tag.lower() != "s":
        subprocess.run(['svn', 'cp', 'trunk/', tag])
    
        print("############ New tag created ##########")
        # 4. commit tag release changes
        message = input("Add an inline commit message: ")
        while message == "":
            message = input("Please add a commit message: ")
        print("Commit message: {}".format(message))
        subprocess.run(['svn', 'ci', "-m'{}'".format(message)])
        print("########## Message Commited ########## ")
