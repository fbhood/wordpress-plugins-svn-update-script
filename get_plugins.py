#!/usr/bin/env python3.7
import os


def get_wp_plugins_list(path_wp_plugins):
    """ ### Returns a list of plugins under svn with wordpress

    path_wp_plugins:str = A string with the absolute path to the wp_plugins folder

    return list"""
    plugins_path_list = []
    # get current directories
    wp_repo_path = os.chdir(path_wp_plugins)
    folders = os.listdir(wp_repo_path)
    print('''Analyzed the following folders:
        {} '''.format(folders))

    # check if under svn (see if Wordpress.org folder exists)
    for element in folders:
        if os.path.isfile(element):
            continue
        else:
            #print(""" ########################################################################
            # Printing folders/files for {} """.format(element))
            # change to the folder
            list_folders = os.listdir(element)
            list_folders_lower = [el.lower() for el in list_folders]
            if "WordPress.org".lower() in list_folders_lower:
                plugins_path_list.append(element)
    print("I found {} plugins under SVN with WordPress.org: {}".format(
        len(plugins_path_list), plugins_path_list))
    return plugins_path_list


def get_wp_org_plugin_path(folder):
    """ Get the absolute path to the wordpress.org folder

        folder:str = the current plugin folder

        return: str = returns the full path to the wordpress.org folder
     """
    print(folder)
    plugin_dirs = [el.lower()
                   for el in os.listdir(folder) if os.path.isdir]
    print(plugin_dirs)
    folder_index = plugin_dirs.index("wordpress.org")
    wp_org_folder = "{}/{}".format(
        folder, os.listdir(folder)[folder_index])
    os.chdir(wp_org_folder)
    cwd = os.getcwd()

    return cwd
