#!/usr/bin/env python3.7
import subprocess


def sync_files(exclude_path: str, folder_to_sync: str, destination_folder: str):
    """ Syncs files using rsync 

    exclude_path:str = path to the exclude-list.txt file in the plugin repository
    folder_to_sync:str = path to the folder where the source code is located
    destination_folder:str = path to the destination folder

    The function executes the following command: 
    `
    subprocess.run(["rsync", "-Erv", "--exclude-from",
                exclude_path, folder_to_sync, destination_folder])
    `
    """
    # sync files inide the truck folder

    # 5. sync files with rsync
    print("Paths for the rsync command: ")
    print("""\n
    {}
    {}
    {}
    """.format(exclude_path, folder_to_sync, destination_folder))

    print("""
       The following rsync command will be executed:
       rsync -Erv --exclude {} {} {}
       """.format(exclude_path, folder_to_sync, destination_folder))

    print("I am syncing the folders in {} to {}".format(
        folder_to_sync, destination_folder))
    subprocess.run(["rsync", "-Erv", "--exclude-from",
                    exclude_path, folder_to_sync, destination_folder])
    print("######## Syncronization complete! #########")
