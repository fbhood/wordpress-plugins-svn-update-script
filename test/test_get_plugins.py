#!/usr/bin/env python3.7
import unittest
from get_plugins import get_wp_plugins_list


class TestGetPlugins(unittest.TestCase):
    def test_basic(self):
        path = "/mnt/c/Users/FabioHood/projects/Repos/wp_plugins"
        test_case = get_wp_plugins_list(path)
        self.assertIsInstance(test_case, list)


if __name__ == '__main__':
    unittest.main()
