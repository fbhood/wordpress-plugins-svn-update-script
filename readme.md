# Wordpress.org SVN plugins update script
This program is intended to automate the process of pushing and tagging new plugins releases to the Wordpress.org repository.


## How to execute the program
To run the program simply execute the wp_repos_update.py file, make sure it is executable

```bash
./wp_repos_update.py --help
```

## How it works
When the program is executed it asks to select between two options: 
- [1] add to the trunk and commit, 
- [2] add to the tunk, tag the release and commit.
- [q] quit
The program scans a given folder and looks for plugins submitted to the Wordpres.org repo. It is configured to look for a folder inside a plugin called Wordpress.org where the folder under svn must be located. 
for example: /projects/plugins/my-plugin/Wordpress.org/plugin-name

The plugin-name inside the Wordpress.org folder must be the name the plugin has in the official Wordpress repository.

