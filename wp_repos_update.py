#!/usr/bin/env python3.7


import sys
import os
from get_plugins import get_wp_plugins_list, get_wp_org_plugin_path
from do_sync import sync_files
from svn_ops import trunk_and_commit, trunk_tag_release_commit


def main():

    if len(sys.argv) > 1:
        if sys.argv[1] == "--help":
            print("""

                ################################### Help ####################################

                [1]: Sync/Add to the truck and commit
                [2]: Sync/Add/Tag and commit
                [Q]: Quit
                """)
    else:
        print("error: run the command using --help ")
        sys.exit(1)

    running = True

    while running:
        select_option = input("Select one of the options [1/2/q]")
        if select_option.lower() == "q":
            print("Bye Bye!")
            running = False
        else:

            print("""Type below the WordPress plugins path where look for repositories under svn.
                    Default path: /mnt/c/Users/FabioHood/projects/Repos/wp_plugins
                """)
            # Get plugins paths
            plugins_path = input("Type the path to the WP plugins: ")
            if plugins_path == "":
                plugins_path = "/mnt/c/Users/FabioHood/projects/Repos/wp_plugins"
            plugins = ["{}/{}".format(plugins_path, el)
                       for el in get_wp_plugins_list(plugins_path)]
            print("Plugins full path: {}".format(plugins))

            print("Prepare to sync files and folders...")
            print(plugins)

            # Sync files

            for folder in plugins:
                # 2. get the path to the exclude file
                exclude_path = "{}/exclude-list.txt".format(folder)

                # 3. get the path to sync
                folder_to_sync = "{}/".format(folder)
                print("##### Printing the contents of the Folder to sync ####")
                print(os.listdir(folder_to_sync))
                # 4. get the path to the trunk folder

                wp_org_folder = get_wp_org_plugin_path(folder)
                print("This is the current plugin folder: " + os.listdir(wp_org_folder)[0])
                destination_folder = "{}/{}/trunk/".format(
                    wp_org_folder, os.listdir(wp_org_folder)[0])

                sync_files(exclude_path, folder_to_sync, destination_folder)
                print("Syncronization of {} complete".format(folder))
                if select_option == "1":
                    print("######## Sync the Trunk and commit ########## ")
                    trunk_and_commit(folder)

                elif select_option == "2":
                    print(
                        "######## Sync the Trunk, tag a new release and commit ########## ")
                    trunk_tag_release_commit(folder)


if __name__ == '__main__':
    main()
